#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <stdlib.h>

int main () {
    WINDOW *ablak;
    ablak = initscr (); 
	
    int x = 0;
	int y = 0;
	int xnov = 0;
    int ynov = 0;
    int mx;
    int my;

	getmaxyx ( ablak, my , mx );

    for ( ;; ) { 
		x = (x-1)%mx;
		xnov = (xnov+1)%mx;
		y = (y-1)%my;
		ynov = (ynov+1)%my;
		clear();
        mvprintw (abs((y+(my-ynov))), abs((x+(mx-xnov))),"o");
        refresh ();
        usleep ( 1000000 ); 
	}
    return 0;
}