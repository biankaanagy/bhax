#include <stdlib.h>
#include <stdio.h>

int main() {
    int a = 6;
    int b = 9;

    a = a * b;
    b = a / b;
    a = a / b;

    printf("a = %d\nb = %d\n", a, b);
}