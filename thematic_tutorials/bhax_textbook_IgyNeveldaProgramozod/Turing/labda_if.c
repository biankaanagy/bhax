//Labdapattogtatás if segítségével 

#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main(void) {
	WINDOW *ablak;
	ablak = initscr();

	int x = 0;
	int y = 0;

	int xnov = 1;
	int ynov = 1;

	int mx;
	int my;

	for(;;){
		getmaxyx (ablak, my, mx);

		mvprintw(y, x, "O");

		refresh();
		usleep(100000);
		clear();

		x += xnov;
		y += ynov;

		if (x >= mx - 1) {
			xnov *= -1;
		}

		if (x <= 0) {
			xnov *= -1;
		}

		if (y <= 0) {
			ynov *= -1;
		}

		if (y >= my - 1) {
			ynov *= -1;
		}

	}

	return 0;
}