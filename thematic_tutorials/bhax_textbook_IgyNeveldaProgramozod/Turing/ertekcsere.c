//Értékcsere segédváltozó nélkül

#include <stdlib.h>
#include <stdio.h>

int main() {
	int a = 6;
	int b = 9;

	b = b - a;
	a = a + b;
	b = a - b;

	printf("a = %d\nb = %d\n", a, b);
}