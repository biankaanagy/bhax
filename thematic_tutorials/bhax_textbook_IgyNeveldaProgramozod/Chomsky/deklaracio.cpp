#include <iostream>
using namespace std;

int *h() {
		int a = 33;
		int *b = &a;
		cout << b << "\n";
		return b;
	}

int main () {
	//egész
	int a = 33;
	cout << "Egész: " << a << "\n";
	cout << "\n";

	//egészre mutató mutató
	int *b = &a;
	cout << "Egészre mutató mutató: " << b << "\n";
	cout << "\n";

	//egész referencia
	int &r = a;
	cout << "Egész referenciája: " << a << "\n";
	cout << "\n";

	//egészek tömbje
	int c[5] = {1, 2, 3, 4, 5};
	for (int i = 0; i < 5; i++) {
		cout << "Egészek tömbje: " << c[i] << "\n";
	}
	cout << "\n";

	//egészek tömbjének referenciája(nem az első elemé)
	int (&tr)[5] = c;
	cout << "Egészek tömbjének referenciája: " << tr << "\n";
	cout << "\n";

	//egészre mutató mutatók tömbje
	int *d[5];
	for (int i = 0; i < 5; i++) {
		d[i] = &c[i];
		cout << "Egészre mutató mutatók tömbje: " << d[i] << "\n";
	}
	cout << "\n";

	//egészre mutató mutatót visszaadó függvény
	cout << "Egészre mutató mutatót visszaadó függvény: " << h() << "\n";
	cout << "\n";

	//egészet mutató mutatót visszaadó függvényre mutató mutató
	int *(*pointer)() = h;
	cout << "Egészet mutató mutatót visszaadó függvényre mutató mutató: " << reinterpret_cast<void*>(pointer) << "\n";

	return 0;
}