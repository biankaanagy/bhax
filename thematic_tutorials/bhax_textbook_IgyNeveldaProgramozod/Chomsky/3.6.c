#include <signal.h>
#include <stdio.h>
#include <stdlib.h>


int f(int a,int b){
	return a+b;
}

int g(int a) {

	return ++a;
}

int h(int a) {

	return ++a;
}

int main() {
	int i;
	int a = 15;
	int tomb[5];
	int *s = &a; 
	int *d = &a;
	int n = 5;
	

	for(i = 0; i < 5; tomb[i] = i++)
		printf(" %d, ", tomb[i]);
	printf("\n");

	for(i = 0; i < n && (*d++ = *s++); ++i)
	printf("\n");

	printf("%d %d",f(a, ++a), f(++a, a));
	printf("\n");
	
	return 0;
}
